﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameMode : MonoBehaviour {

    private GameMode instance = null;

    public GameObject gameUIObj;
    public GameObject playerObj;

    protected GameUI gameUI = null;
    protected Player player = null;

    protected virtual void Awake()
    {
        if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
        else
        {
            instance = this;
        }

        if (gameUIObj == null)
        {
            try
            {
                gameUI = GameObject.Find("GameUI").GetComponent<GameUI>();
            }
            catch (Exception)
            {
                Debug.Log("please add GameUI");
            }

        }
        else
        {
            GameObject gameUITemp = Instantiate(gameUIObj);
            gameUITemp.name = "GameUI";
            gameUI = gameUITemp.GetComponent<GameUI>();
        }

        if (player == null)
        {
            try
            {
                player = GameObject.Find("Player").GetComponent<Player>();
            }
            catch (Exception)
            {
                Debug.Log("please add Player");
            }

        }
        else
        {
            GameObject playerTemp = Instantiate(playerObj);
            playerTemp.name = "Player";
            player = playerTemp.GetComponent<Player>();
        }

        gameUI.SetupGameUI(this);
        player.SetupPlayer(this);
    }
    
    public Player GetPlayer()
    {
        return player;
    }

    public GameUI GetGameUI()
    {
        return gameUI;
    }

    public virtual void GameOver()
    {

    }

    public virtual void AddScore(int amount)
    {

    }

}
