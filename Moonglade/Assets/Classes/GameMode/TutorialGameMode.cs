﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGameMode : GameMode {

    private int score;

    private void Start()
    {
        StartTutorial();
    }

    public void StartTutorial()
    {
        GameInstance.SetGameState(GameState.PLAYING);
        score = 0;
        LevelGameUI gameUI = (LevelGameUI)this.gameUI;
        gameUI.OnScoreChange(score);
    }

    public override void GameOver()
    {
        base.GameOver();

        DotPlayer player = (DotPlayer) this.player;
        player.RecenterCamera();
    }

    public void ToMenu()
    {
        GameInstance.SetGameState(GameState.MENU);
        GameInstance.LoadLevel("menu");
    }
}
