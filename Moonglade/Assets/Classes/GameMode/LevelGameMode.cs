﻿using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;

public class LevelGameMode : GameMode
{
    public List<GameObject> obstacles;
    public int obstaclesSpawned = 2;

    public float YEndPoint;
    public float distanceBetweenObs = 5f;
    public int maxObsSpawned = 5;

    [SerializeField]
    private List<Obstacle> obs;
    private Obstacle lastObstacle;

    private int score = 0;
    private int playCount = 0;

    protected override void Awake()
    {
        base.Awake();

        foreach (GameObject obs in obstacles)
        {
            for (int x = 0; x < obstaclesSpawned; x++)
            {
                GameObject go = Instantiate(obs, transform);
                this.obs.Add(go.GetComponent(typeof(Obstacle)) as Obstacle);
                go.SetActive(false);
            }
        }
    }

    public void ToMenu()
    {
        GameInstance.SetGameState(GameState.MENU);
        GameInstance.LoadLevel("menu");
    }

    private void Start()
    {
        
        StartLevel();
    }

    public override void AddScore(int amount)
    {
        score += amount;
        LevelGameUI gameUI = (LevelGameUI)this.gameUI;
        gameUI.OnScoreChange(score);
    }

    public int GetScore()
    {
        return score;
    }
    
    public void StartLevel()
    {
        playCount = PlayerPrefs.GetInt("total_play") + 1;
        PlayerPrefs.SetInt("total_play", playCount);

#if UNITY_EDITOR
        //first Obstacle
        foreach (Obstacle obsCollection in obs)
        {
            obsCollection.gameObject.SetActive(false);
        }

        score = 0;
        LevelGameUI gameUI = (LevelGameUI)this.gameUI;
        gameUI.OnScoreChange(score);
        Obstacle firstObs = SetObstacle(obs[0], new Vector2(0, 3));
        lastObstacle = firstObs;
        for (int x = 0; x < maxObsSpawned; x++)
        {
            Obstacle spawnedObs = SetObstacle();
            lastObstacle = spawnedObs;
        }

        DotPlayer player = (DotPlayer)this.player;
        player.GetRigidBody().simulated = true;
        player.transform.position = new Vector2(0, -3.1f);

        Camera.main.transform.position = new Vector3(0, 1, -10);
        gameUI.OnGamePlay();

        GameInstance.SetGameState(GameState.PLAYING);
#else
        if (GameAds.CanDoAction())
        {
            //first Obstacle
            foreach (Obstacle obsCollection in obs)
            {
                obsCollection.gameObject.SetActive(false);
            }

            score = 0;
            LevelGameUI gameUI = (LevelGameUI)this.gameUI;
            gameUI.OnScoreChange(score);
            Obstacle firstObs = SetObstacle(obs[0], new Vector2(0, 3));
            lastObstacle = firstObs;
            for (int x = 0; x < maxObsSpawned; x++)
            {
                Obstacle spawnedObs = SetObstacle();
                lastObstacle = spawnedObs;
            }

            DotPlayer player = (DotPlayer)this.player;
            player.GetRigidBody().simulated = true;
            player.transform.position = new Vector2(0, -3.1f);

            Camera.main.transform.position = new Vector3(0, 1, -10);
            gameUI.OnGamePlay();

            GameInstance.SetGameState(GameState.PLAYING);
        }
#endif


    }

    public override void GameOver()
    {
        GameInstance.SetGameState(GameState.PAUSE);
        DotPlayer player = (DotPlayer)this.player;
        player.GetRigidBody().simulated = false;
        LevelGameUI gameUI = (LevelGameUI)this.gameUI;

        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ReportScore(score,
                GPGSIds.leaderboard_classic,
                (bool success) =>
                {
                    Debug.Log("(Color Switch) gplayserv Leaderboard update success: " + success);
                });
        }

        gameUI.OnGameOver();
        foreach (Obstacle obstacle in obs)
        {
            obstacle.Remove();
        }

        if (playCount > 2)
        {
            GameAds.ShowAds();
        }
    }

    private Obstacle SetObstacle(Obstacle obs, Vector2 position)
    {
        int randomIndexObs = Random.Range(0, this.obs.Count - 1);
        Obstacle chosenObs = this.obs[randomIndexObs];
        if (chosenObs.gameObject.activeSelf)
        {
            return SetObstacle(obs, position);
        }
        chosenObs.gameObject.transform.position = position;
        chosenObs.gameObject.SetActive(true);
        lastObstacle = chosenObs;
        chosenObs.Setup();
        return chosenObs;
    }

    public Obstacle SetObstacle()
    {
        int randomIndexObs = Random.Range(0, this.obs.Count - 1);
        Obstacle chosenObs = obs[randomIndexObs];
        if (chosenObs.gameObject.activeSelf)
        {
            return SetObstacle();
        }
        else
        {
            Vector2 toSpawn = Vector2.zero;
            toSpawn.y = lastObstacle.transform.position.y + lastObstacle.radius + distanceBetweenObs + chosenObs.radius;
            chosenObs.gameObject.transform.position = toSpawn;
            chosenObs.gameObject.SetActive(true);
            lastObstacle = chosenObs;
            chosenObs.Setup();
            return chosenObs;
        }

    }

}
