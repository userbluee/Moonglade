﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Obstacle : MonoBehaviour
{
    private SpriteRenderer[] obstacles;
    private GameMode gameMode;

    public float radius;
    public List<BlackAndWhiteElement> blackWhiteElements;
    public bool randomSpeed = false;

    private DotPlayer player;

    protected virtual void Awake()
    {
        obstacles = GetComponentsInChildren<SpriteRenderer>();
        gameMode = GameInstance.GetGameMode();
    }

    protected virtual void Start()
    {
        player = (DotPlayer)gameMode.GetPlayer();
        player.AddBlackWhiteElement(blackWhiteElements);
    }

    public void Setup()
    {
        
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
        gameObject.BroadcastMessage("SetupObstacle");
    }

    public virtual void SetupObstacle()
    {
        DotPlayer player = (DotPlayer)gameMode.GetPlayer();
        foreach (BlackAndWhiteElement element in blackWhiteElements)
        {

            element.spriteRef.DOFade(1, 0);
            if (player.playerInformation.IsBlack())
            {
                element.ChangeToBlack();
            }
            else
            {
                element.ChangeToWhite();
            }

            try
            {
                element.spriteRef.GetComponent<Collider2D>().enabled = true;
            }
            catch
            {
                Debug.Log("NO COLLISION BOX");
            }
        }
        
    }

    protected virtual void Update()
    {
        float distance = transform.position.y - player.transform.position.y;

        if (distance < -10f)
        {
            Remove();
        }
    }

    public void Remove()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
            try
            {
                LevelGameMode gamemode = (LevelGameMode)this.gameMode;
                gamemode.SetObstacle();
            }
            catch
            {
            }
        }
    }
    
}
