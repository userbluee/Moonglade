﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Player instance;
    protected GameMode gameMode;

    protected virtual void Awake()
    {
        if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
        else
        {
            instance = this;
        }
    }

    public void SetupPlayer(GameMode gameMode)
    {
        this.gameMode = gameMode;
    }
    /*
    protected virtual void Start()
    {
        gameMode = GameInstance.GetGameMode();
    }
    */
}
