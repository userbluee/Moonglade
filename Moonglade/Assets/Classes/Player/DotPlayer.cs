﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DotPlayer : Player
{

    public List<BlackAndWhiteElement> blackWhiteElements;
    public BlackAndWhiteElement playerInformation;
    public float force = 100;
    public float maxSpeed;

    private bool isTouch = false;
    private Rigidbody2D rb2D;

    private TutorialCheck currentCheckpoint;

    protected override void Awake()
    {
        base.Awake();

        rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (GameInstance.GetGameState() == GameState.PLAYING)
        {
            
            if (Input.GetButton("Fire1") && !isTouch)
            {
                isTouch = true;
                Vector2 force = new Vector2(0, this.force);
                rb2D.AddForce(force);
                GameAudio.PlaySFX(SFXName.TOUCH);
                AdjustElement();

                if(Time.timeScale == 0)
                {
                    CheckTutorial();
                }
            }
            if (Input.GetButtonUp("Fire1") && isTouch)
            {
                isTouch = false;
            }

            Vector2 cameraPoint = Camera.main.WorldToScreenPoint(transform.position);
            float middleScreen = Camera.main.pixelHeight / 2;
            if (cameraPoint.y > middleScreen)
            {
                RecenterCamera();
                /*
                Vector3 movementCamera = transform.position;
                movementCamera.z = -10;
                Camera.main.transform.position = movementCamera;*/
            }

            if (cameraPoint.y < 0)
            {
                gameMode.GameOver();
            }

            if (rb2D.velocity.magnitude > maxSpeed)
            {
                rb2D.velocity = rb2D.velocity.normalized * maxSpeed;
            }
        }

    }

    public void RecenterCamera()
    {
        Vector3 movementCamera = transform.position;
        movementCamera.z = -10;
        Camera.main.transform.position = movementCamera;
    }

    private void CheckTutorial()
    {
        if (Input.mousePosition.x < Camera.main.pixelWidth / 2)
        {
            //change white
            if (!currentCheckpoint.rightSideTap)
            {
                currentCheckpoint.Go();
            }
            
        }
        else
        {
            //change to black
            if (currentCheckpoint.rightSideTap)
            {
                currentCheckpoint.Go();
            }
        }
    }

    private void AdjustElement()
    {
        LevelGameUI gameUI = (LevelGameUI)gameMode.GetGameUI();
        if (Input.mousePosition.x < Camera.main.pixelWidth / 2)
        {
            //change white
            if (playerInformation.IsBlack())
            {
                playerInformation.ChangeElement();
                foreach (BlackAndWhiteElement element in blackWhiteElements)
                {
                    element.ChangeElement();
                }
                
                gameUI.OnChangeElement();
                
            }
            
            if (currentCheckpoint)
            {
                gameUI.tutorialInstruction.color = Color.white;
            }
        }
        else
        {
            //change to black
            if (!playerInformation.IsBlack())
            {
                playerInformation.ChangeElement();
                foreach (BlackAndWhiteElement element in blackWhiteElements)
                {
                    element.ChangeElement();
                }
               
                gameUI.OnChangeElement();
                
            }
            
            if (currentCheckpoint)
            {
                gameUI.tutorialInstruction.color = Color.black;
            }
        }
    }

    public void AddBlackWhiteElement(List<BlackAndWhiteElement> newElement)
    {
        foreach (BlackAndWhiteElement element in newElement)
        {
            blackWhiteElements.Add(element);
        }
    }

    public Rigidbody2D GetRigidBody()
    {
        return rb2D;
    }
    
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (playerInformation.IsBlack() && collision.tag == "Black")
        {
            try
            {
                LevelGameMode gameMode = (LevelGameMode)this.gameMode;
                GameAudio.PlaySFX(SFXName.LOSE);
                gameMode.GameOver();
            }
            catch
            {
                transform.position = currentCheckpoint.transform.position;
                currentCheckpoint.Freeze("Be Careful! Do not to hit any obstacles!");
            }

        }

        if (!playerInformation.IsBlack() && collision.tag == "White")
        {
            try
            {
                LevelGameMode gameMode = (LevelGameMode)this.gameMode;
                GameAudio.PlaySFX(SFXName.LOSE);
                gameMode.GameOver();
            }
            catch
            {
                transform.position = currentCheckpoint.transform.position;
                currentCheckpoint.Freeze("Be Careful! Do not to hit any obstacles!");
            }
            
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag == "Score")
        {
            Transform lastData = collision.transform;
            
            collision.enabled = false;

            Sequence destroyScore = DOTween.Sequence();
            destroyScore.Append(collision.transform.DOShakePosition(0.2f, 0.7f, 10));
            destroyScore.Insert(0, collision.GetComponent<SpriteRenderer>().DOFade(0, 0.5f));
            destroyScore.Append(collision.transform.DOMove(lastData.position, 0));
            destroyScore.Play();

            GameAudio.PlaySFX(SFXName.SCORE);
            gameMode.AddScore(1);
        }

        if (collision.tag == "GameController")
        {
            currentCheckpoint = collision.GetComponent<TutorialCheck>();
            AdjustElement();
            currentCheckpoint.Freeze();
        }
    }
}

[System.Serializable]
public struct BlackAndWhiteElement
{
    public Sprite blackSprite;
    public Sprite whiteSprite;

    public SpriteRenderer spriteRef;

    public void ChangeElement()
    {
        if (spriteRef.sprite == blackSprite)
        {
            spriteRef.sprite = whiteSprite;
        }
        else
        {
            spriteRef.sprite = blackSprite;
        }
    }

    public void ChangeToBlack()
    {
        spriteRef.sprite = blackSprite;
    }

    public void ChangeToWhite()
    {
        spriteRef.sprite = whiteSprite;
    }

    public bool IsBlack()
    {
        if (spriteRef.sprite == blackSprite)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
