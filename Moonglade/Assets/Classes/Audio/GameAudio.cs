﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameAudio : MonoBehaviour
{

    public AudioSource musicSource;
    public AudioSource sfxSource;
    public AudioSource playerSource;

    public AudioClip musicClip;
    public List<SFX> SFXList;

    public static GameAudio instance { get; set; }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        PlayMusic();   
    }

    private void PlayMusic()
    {
        musicSource.clip = musicClip;
        musicSource.loop = true;
        musicSource.Play();
    }

    public static void PlaySFX(SFXName name)
    {
        AudioClip clip = null;
        foreach (SFX sfx in instance.SFXList)
        {
            if (sfx.name == name)
            {
                clip = sfx.clip;
                break;
            }
        }

        if (clip != null)
        {
            AudioSource sourceReference;

            if (name == SFXName.TOUCH)
            {
                sourceReference = instance.playerSource;
            }
            else
            {
                sourceReference = instance.sfxSource;
            }

            sourceReference.clip = clip;
            sourceReference.loop = false;
            sourceReference.Play();
        }
        else
        {
            Debug.Log("Please add sfx " + name);
        }
    }

    public static void ToggleSFX(bool play)
    {
        if (!play)
        {
            if (instance.sfxSource.volume > 0f && instance.playerSource.volume > 0f)
            {
                instance.sfxSource.volume = 0f;
                instance.playerSource.volume = 0f;
            }
        }
        else
        {
            if (instance.sfxSource.volume == 0f && instance.playerSource.volume == 0f)
            {
                instance.sfxSource.volume = 1f;
                instance.playerSource.volume = 1f;
            }
        }
    }

    public static void ToggleMusic(bool play)
    {
        if (!play)
        {
            if (instance.musicSource.isPlaying)
            {
                instance.musicSource.Stop();
            }
        }
        else
        {
            if (!instance.musicSource.isPlaying)
            {
                instance.PlayMusic();
            }
        }
    }

    public static bool IsSFXOn()
    {
        if (instance.sfxSource.volume > 0f && instance.playerSource.volume > 0f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool IsMusicOn()
    {
        if (instance.musicSource.isPlaying)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

[System.Serializable]
public struct SFX
{
    public SFXName name;
    public AudioClip clip;
}

[System.Serializable]
public enum SFXName
{
    TOUCH, LOSE, SCORE, SELECT
}