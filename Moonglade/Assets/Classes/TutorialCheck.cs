﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCheck : MonoBehaviour {

    public string instruction;
    public bool finishLine;
    public bool rightSideTap;

    public GameObject checkPoint;

    private bool crossed;

    private LevelGameUI gameUI;
    private TutorialGameMode gameMode;

    private void Awake()
    {
        crossed = false;
        Collider2D col = checkPoint.GetComponent<Collider2D>();
        col.isTrigger = false;
        checkPoint.SetActive(false);
    }

    private void Start()
    {
        gameMode = (TutorialGameMode) GameInstance.GetGameMode();
        gameUI = (LevelGameUI) gameMode.GetGameUI();
    }

    public void Freeze(string instruction)
    {
        gameUI.OnUITutorial(rightSideTap, instruction);
    }

    public void Freeze()
    {
        if (!crossed && !finishLine)
        {
            Time.timeScale = 0;
            crossed = true;
            gameUI.OnUITutorial(rightSideTap, instruction);
        }
        else if (!crossed && finishLine)
        {
            crossed = true;
            GameInstance.SetGameState(GameState.PAUSE);
            gameUI.OnTutorialComplete();
        }
        
    }

    public void Go()
    {
        Time.timeScale = 1;
        checkPoint.SetActive(true);
    }
}
