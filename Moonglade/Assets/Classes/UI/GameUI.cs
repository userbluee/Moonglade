﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameUI : MonoBehaviour
{
    private GameUI instance = null;
    protected GameMode gameMode;
    
    protected virtual void Awake()
    {
        if (instance != null)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
        else
        {
            instance = this;
        }
    }

    public void SetupGameUI(GameMode gameMode)
    {
        this.gameMode = gameMode;
    }
}
