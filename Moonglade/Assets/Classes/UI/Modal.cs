﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Modal : MonoBehaviour {
    
    public Button yesButton;
    public Button noButton;
    public Text message;

    public delegate void ModalAction();
    
    public void ShowModal(string message, ModalAction yes, ModalAction no)
    {
        
        yesButton.onClick.RemoveAllListeners();
        noButton.onClick.RemoveAllListeners();

        this.message.text = message;

        yesButton.onClick.AddListener(delegate
        {
            yes();
            HideModal();
        });
        noButton.onClick.AddListener(delegate
        {
            no();
            HideModal();
        });

        gameObject.SetActive(true);

    }

    public void HideModal()
    {
        yesButton.onClick.RemoveAllListeners();
        noButton.onClick.RemoveAllListeners();
        gameObject.SetActive(false);
    }

}
